# Bali Trip Backend

## Introduction


### 1. Base Url
  ```php        
http://HOST/api/v1/
```
### 2. Autehntication [POST]
`Key : email, password` 

`Response: json`
  ```php
http://HOST/api/v1/auth
  ```
### 3. Signout [POST]
`Key : token`

`Response: json`
  ```php
http://HOST/api/v1/signout
  ```
### 4. Register [POST]
`Key : name, email, password` 

`Response: json`
  ```php
http://HOST/api/v1/register
  ```
### 5. Get all discoveries [GET]
`Key : token` 

`Response: json`
  ```php
http://HOST/api/v1/discovery/all/{token}
  ```
### 6. Get hot discoveries [GET]
`Key : token` 

`Response: json`
  ```php
http://HOST/api/v1/discovery/hot/{token}
  ```
### 7. Get all detail discovery [GET]
`Key : id, token` 

`Response: json`
  ```php
http://HOST/api/v1/discovery/detail/{id}/{token}
  ```
### 8. Add discoveries [POST]
`Key : title, category, description, token` 

`Response: json`
  ```php
http://HOST/api/v1/discovery//{token}
  ```
## License

A short snippet describing the license (MIT, Apache, etc.)
