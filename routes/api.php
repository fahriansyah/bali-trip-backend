<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|--------------------------------------------------------------------------
| API Routes V1 
|--------------------------------------------------------------------------
| Routing untuk API request
|
| base_url : /api/v1/
*/

Route::prefix('v1')->group(function(){
	/*
	|--------------------------------------------------------------------------
	| Routes Authentication
	|--------------------------------------------------------------------------
	| Route ini untuk autentikasi
	| auth, signout, register akun user
	|
	*/
	Route::post('/auth', 'Api\AuthController@authenticate');
	Route::get('/signout/{token}', 'Api\AuthController@signout');
	Route::post('/register', 'Api\AuthController@register');

	/*
	|--------------------------------------------------------------------------
	| Discoveries routes
	|--------------------------------------------------------------------------
	| Route ini untuk discoveries
	| 
	| all : mengambil semua discovery dengan paginasi 12	{@param : token(string)}
	| hot : mengambil semua hot discovery 					{@param : token(string)}
	| detail : mengambil spesifik discovery 				{@param : id(integer), token(string)}
	| add : menambah kan data discovery 					{@param : token(string)}
	|
	*/
	Route::get('/discovery/all/{token}', 'Api\DiscoveriesController@getAllDiscoveries');
	Route::get('/discovery/hot/{token}', 'Api\DiscoveriesController@getHotDiscoveries');
	Route::get('/discovery/detail/{id}/{token}', 'Api\DiscoveriesController@getDetailDiscovery');
	Route::post('/discovery/add/{token}', 'Api\DiscoveriesController@addDiscoveries');
});