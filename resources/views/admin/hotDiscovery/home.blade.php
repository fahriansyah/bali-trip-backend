@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
          <div class="card-header">Hot Discovery</div>

          <div class="card-body">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Hot discovery</button><br><br>
              <table class="table table-sriped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>fahriansyah</td>
                    <td>South jakarta</td>
                    <td>Travelling</td>
                    <td>
                      <a href="#" class="btn btn-danger">Delete</a>
                      <a href="#" class="btn btn-light">Detail</a>
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <!-- <span aria-hidden="true">&times;</span> -->
          <!-- <span class="sr-only">Close</span> -->
        </button>
        <h4 class="modal-title">Add hot discoveries</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-12">
              <label>Chose discoveries</label>
              <select class="form-control-md">
                <option value="1">1</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
