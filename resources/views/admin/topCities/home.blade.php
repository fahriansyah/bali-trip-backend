@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
          <div class="card-header">Hot Discovery</div>

          <div class="card-body">
            <button class="btn btn-primary">Add Hot discovery</button><br><br>
              <table class="table table-sriped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>fahriansyah</td>
                    <td>South jakarta</td>
                    <td>Travelling</td>
                    <td>
                      <a href="#" class="btn btn-danger">Delete</a>
                      <a href="#" class="btn btn-light">Detail</a>
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
