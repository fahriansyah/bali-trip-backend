<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotDiscoveries extends Model
{
    //
    protected $table = "hot_discoveries";

    protected $primaryKey = "discovery_id";

    public static function discoveries()
    {
    	$data = [];
    	foreach (HotDiscoveries::all() as $key => $value) {
    		$data[] = Discoveries::find($value->discovery_id);
    	}
    	return $data;
    }

    public function findDiscovery()
    {
    	$discoveries = Discoveries::find($this);
    	return $discoveries;
    }
}
