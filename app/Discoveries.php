<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Discoveries extends Model
{
    protected $table = "discoveries";
    protected $primaryKey = "id";

    protected $hidden = [
    	'user_id'
    ];
}
