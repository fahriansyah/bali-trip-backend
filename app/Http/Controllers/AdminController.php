<?php

namespace App\Http\Controllers;

use App\Discoveries;
use App\HotDiscoveries;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
    	# code...
    }

    public function index()
    {
    	
    }

    public function addHotDiscoveries(Request $r)
    {
    	$hotDiscoveries = new HotDiscoveries;
    	$hotDiscoveries->discoveries_id = $r->discoveries_id;
    	$hotDiscoveries->save();

    	return back();
    }
}
