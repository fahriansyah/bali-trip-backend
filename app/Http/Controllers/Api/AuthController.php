<?php

namespace App\Http\Controllers\Api;

use Auth;
use Hash;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    //
    public function authenticate(Request $r)
    {
    	$credentials = ["email" => $r->email, "password" => $r->password];

    	if (Auth::guard('web')->once($credentials)) {
    		
    		$token = str_random(32);
    		User::where('email', $r->email)->update(['token' => $token]);
    		
    		return response()->json([
    			'message' 	=> 'OKE!',
    			'status' 	=> 200,
    			'data' 		=> User::where('email', $r->email)->first()
    		], 200);
    	}

    	return response()->json([
    		'message' 	=> 'wrong username or password',
    		'status' 	=> 422,
    		'data' 		=> null
    	], 200);
    }

    public function signout($token)
    {

    	if ($user = User::find($token)) {
		    
		    $user->token = null;
		    $user->save();

		    return response()->json([
	    		'message' => 'OKE!',
	    		'status' => 200,
	    		'data' => null
	    	]);

    	}else{
    		
    		return response()->json([
    			'message' 	=> 'user has been logout',
    			'status' 	=> 422,
    			'data' 		=> null
    		]);

    	}
    }

    public function register(Request $r)
    {
    	$validator = Validator::make($r->all(),[
    		'name' 		=> 'required|string|max:100',
    		'email' 	=> 'required|email|max:100|unique:users,email',
    		'password' 	=> 'required|string|max:100'
    	]);

    	if ($validator->fails()) {
    		return response()->json([
    			'message' 	=> 'something went wrong',
    			'status' 	=> 422,
    			'errors'	=> $validator->errors(),
    		], 200);
    	}

    	$user = new User;
    	$user->name = $r->name;
    	$user->email = $r->email;
    	$user->password = Hash::make($r->password);
    	$user->save();

    	return response()->json([
    		'message' 	=> 'OKE!',
    		'status' 	=> 200,
    		'data' 		=> $user
    	]);
    }
}
