<?php

namespace App\Http\Controllers\Api;

use App\User;
use Validator;
use App\Discoveries;
use App\HotDiscoveries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscoveriesController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth.api.user');
    }

    public function getAllDiscoveries()
    {
        $data = [];
    	foreach (Discoveries::all() as $key => $value) {
    		$data[] = [
                'id'        => $value->id,
                'title'     => $value->title,
                'category'  => $value->category
            ];
    	}
        return response()->json([
            'message'    => 'OKE!',
            'status'     => 200,
            'data'       => $data
        ], 200);
    }

    public function getHotDiscoveries()
    {
        // $hotDiscoveries = HotDiscoveries::find(4)->findDiscovery();
        $hotDiscoveries = HotDiscoveries::discoveries();
        return response()->json([
            'message'   => 'OKE!',
            'status'    => 200,
            'data'      => $hotDiscoveries
        ], 200);
    }

    public function getDetailDiscovery($id)
    {
        return response()->json([
            'message' => 'OKE!',
            'status' => 422,
            'data' => Discoveries::find($id)
        ]);
    }

    public function addDiscoveries(Request $r, $token)
    {
        $validator = Validator::make($r->all(),[
            'title'        => 'required|string|max:100',
            'category'     => 'required|string|max:100',
            'description'  => 'required|string|max:1000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message'   => 'something went wrong',
                'status'    => 422,
                'errors'    => $validator->errors(),
            ], 200);
        }

    	$discovery = new Discoveries;
    	$discovery->user_id = User::find($token)->id;
    	$discovery->title = $r->title;
    	$discovery->category = $r->category;
    	$discovery->description = $r->description;
    	$discovery->save();

    	return response()->json([
    		'message' => 'OKE!',
    		'status'  => 200,
    		'data'    => $discovery
    	], 200);
    }
}
