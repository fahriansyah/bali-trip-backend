<?php

namespace App\Http\Middleware;
use App\User;
use Closure;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::find($request->token)){
            return $next($request);
        }

        return response()->json([
            'message' => 'missing or invalid token',
            'status' => 422,
            'data' => null
        ], 200);
    }
}
